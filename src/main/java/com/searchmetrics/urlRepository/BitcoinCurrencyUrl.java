package com.searchmetrics.urlRepository;

import java.util.Date;

public class BitcoinCurrencyUrl implements ICurrencyUrls {

    @Override
    public String getCurrentRatingUrl() {
        return "https://api.coindesk.com/v1/bpi/currentprice.json";
    }

    @Override
    public String getHistoricalRatingUrl(String start, String end) {
        return "https://api.coindesk.com/v1/bpi/historical/close.json?" + getStartLabel(start) + "&" + getEndLabel( end ) ;
    }

    private String getStartLabel(String start) {
        return getQueryString( "start", start );
    }

    private String getEndLabel(String end) {
        return getQueryString( "end", end );
    }

    private String getQueryString(String s, String d) {
        return s + "=" + d;
    }
}
