package com.searchmetrics.urlRepository;

import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * this class fakes a repository (could be a database)
 * of urls for getting the urls where the services can get the ratings
 */
@Repository
public class UrlRepository {

    Map<CurrencyEnum, ICurrencyUrls> mapUrls = Map.of(
            CurrencyEnum.Bitcoin, new BitcoinCurrencyUrl()
    );

    public ICurrencyUrls getUrlOfCurrency(CurrencyEnum currency) {
        final ICurrencyUrls value = this.mapUrls.getOrDefault( currency, null );
        if (value == null) {
            throw new IllegalArgumentException("this currency is not supported.");
        }
        return value;
    }
}
