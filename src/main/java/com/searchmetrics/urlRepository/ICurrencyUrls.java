package com.searchmetrics.urlRepository;

public interface ICurrencyUrls {

    String getCurrentRatingUrl();

    String getHistoricalRatingUrl(String start, String end);
}
