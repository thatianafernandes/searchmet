package com.searchmetrics.services.CurrencyServices.bitcoinDTO;

import com.searchmetrics.Utils.DateUtils;
import com.searchmetrics.model.CurrencyRating;
import com.searchmetrics.services.CurrencyServices.ICurrencyRatingFactory;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import java.util.stream.Collectors;

public class BitcoinCurrencyRatingFactory implements ICurrencyRatingFactory {

    @NotNull
    public CurrencyRating buildCurrencyRating(Object dto) {
        if (dto instanceof BitcoinResponseDTO) {
            BitcoinResponseDTO bitcoinResponseDTO = (BitcoinResponseDTO) dto;
            return convertToCurrencyUSDRating( bitcoinResponseDTO );
        }
        throw new IllegalArgumentException( "Failed converting data currency" );
    }

    @Override
    public List<CurrencyRating> buildListOfHistoricalRatings(Object dto) {

        if (dto instanceof BitcoinHistoricalDTO) {
            BitcoinHistoricalDTO bitcoinHistorical = (BitcoinHistoricalDTO) dto;
            return bitcoinHistorical.getBpi().entrySet().stream().map( entry -> {
                String value = entry.getValue();
                var dateTime = DateUtils.convertFromString( entry.getKey() );
                return new CurrencyRating( dateTime, Double.parseDouble( value ));
            } ).collect( Collectors.toList());


        }
        throw new IllegalArgumentException( "Failed converting data currency" );
    }

    @Override
    public Class getDTOClass() {
        return BitcoinResponseDTO.class;
    }

    @Override
    public Class getDTOListClass() {
        return BitcoinHistoricalDTO.class;
    }

    @NotNull
    private static CurrencyRating convertToCurrencyUSDRating(BitcoinResponseDTO bitcoinResponseDTO) {
        final CurrencyRating currencyRating = new CurrencyRating();
        currencyRating.setRating( bitcoinResponseDTO.getBpi().getUSD().getRate_float() );
        currencyRating.setUtcTimestamp( getUtcTime( bitcoinResponseDTO.getTime().getUpdatedISO() ) );
        return currencyRating;
    }

    private static LocalDateTime getUtcTime(String isoTime) {
        TemporalAccessor ta = DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(isoTime);
        return LocalDateTime.from( ta );
    }


}
