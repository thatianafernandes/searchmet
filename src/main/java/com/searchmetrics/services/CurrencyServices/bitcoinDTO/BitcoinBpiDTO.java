package com.searchmetrics.services.CurrencyServices.bitcoinDTO;

public class BitcoinBpiDTO {

    private BitcoinCurrencyDTO USD;
    private BitcoinCurrencyDTO GBP;
    private BitcoinCurrencyDTO EUR;

    public BitcoinCurrencyDTO getUSD() {
        return USD;
    }

    public void setUSD(BitcoinCurrencyDTO USD) {
        this.USD = USD;
    }

    public BitcoinCurrencyDTO getGBP() {
        return GBP;
    }

    public void setGBP(BitcoinCurrencyDTO GBP) {
        this.GBP = GBP;
    }

    public BitcoinCurrencyDTO getEUR() {
        return EUR;
    }

    public void setEUR(BitcoinCurrencyDTO EUR) {
        this.EUR = EUR;
    }
}
