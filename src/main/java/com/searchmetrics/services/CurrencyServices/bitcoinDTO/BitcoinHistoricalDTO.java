package com.searchmetrics.services.CurrencyServices.bitcoinDTO;

import java.util.Map;

public class BitcoinHistoricalDTO {

    private BitcoinTimeDTO time;
    private String disclaimer;
    private Map<String, String> bpi;

    public BitcoinTimeDTO getTime() {
        return time;
    }

    public void setTime(BitcoinTimeDTO time) {
        this.time = time;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public Map<String, String> getBpi() {
        return bpi;
    }

    public void setBpi(Map<String, String> bpi) {
        this.bpi = bpi;
    }
}
