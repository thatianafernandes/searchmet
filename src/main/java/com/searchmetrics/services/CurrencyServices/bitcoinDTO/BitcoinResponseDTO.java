package com.searchmetrics.services.CurrencyServices.bitcoinDTO;

public class BitcoinResponseDTO  {

    private BitcoinTimeDTO time;
    private String disclaimer;
    private String chartName;
    private BitcoinBpiDTO bpi;

    public BitcoinBpiDTO getBpi() {
        return bpi;
    }

    public void setBpi(BitcoinBpiDTO bpi) {
        this.bpi = bpi;
    }

    public BitcoinTimeDTO getTime() {
        return time;
    }

    public void setTime(BitcoinTimeDTO time) {
        this.time = time;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getChartName() {
        return chartName;
    }

    public void setChartName(String chartName) {
        this.chartName = chartName;
    }

}
