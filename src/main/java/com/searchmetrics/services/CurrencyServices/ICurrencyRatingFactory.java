package com.searchmetrics.services.CurrencyServices;

import com.searchmetrics.model.CurrencyRating;

import java.util.List;

public interface ICurrencyRatingFactory {

    CurrencyRating buildCurrencyRating(Object dto);

    List<CurrencyRating> buildListOfHistoricalRatings(Object dto);


    Class getDTOClass();

    Class getDTOListClass();

}
