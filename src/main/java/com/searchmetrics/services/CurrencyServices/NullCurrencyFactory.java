package com.searchmetrics.services.CurrencyServices;

import com.searchmetrics.model.CurrencyRating;

import java.util.List;

/**
 * Null Object of Currency Factory
 */
public class NullCurrencyFactory implements ICurrencyRatingFactory{
    @Override
    public CurrencyRating buildCurrencyRating(Object dto) {
        throwException();
        return null;
    }

    @Override
    public List<CurrencyRating> buildListOfHistoricalRatings(Object dto) {
        throwException();
        return null;
    }

    private void throwException() {
        throw new IllegalArgumentException( "Failed to find a right converter to this currency" );
    }

    @Override
    public Class getDTOClass() {
        return null;
    }

    @Override
    public Class getDTOListClass() {
        return null;
    }
}
