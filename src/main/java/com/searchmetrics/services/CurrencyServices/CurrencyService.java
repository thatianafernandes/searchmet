package com.searchmetrics.services.CurrencyServices;

import com.google.gson.Gson;
import com.searchmetrics.Utils.HttpClient;
import com.searchmetrics.model.CurrencyRating;
import com.searchmetrics.urlRepository.CurrencyEnum;
import com.searchmetrics.urlRepository.UrlRepository;

import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@CacheConfig(cacheNames = {"bitcoin"})
public class CurrencyService implements IClientCurrencyService {

    @Autowired
    private UrlRepository urlRepository;

    @Autowired
    private CurrenciesAbstractFactory currenciesAbstractFactory;

    @Autowired
    private HttpClient httpClient;

    public CurrencyService() {
    }

    CurrencyService(UrlRepository urlRepository, CurrenciesAbstractFactory currenciesAbstractFactory, HttpClient client) {
        this.urlRepository = urlRepository;
        this.currenciesAbstractFactory = currenciesAbstractFactory;
        this.httpClient = client;
    }

    @Override
    @Cacheable("bitcoin")
    public CurrencyRating getCurrentRating(CurrencyEnum currency)  {
        final ICurrencyRatingFactory factory = getFactory( currency );

        var dtoEntity = deserializeResponse(
                getCurrentRatingOfCurrency( currency ),
                factory.getDTOClass() );

        return factory.buildCurrencyRating( dtoEntity );
    }


    private ICurrencyRatingFactory getFactory(CurrencyEnum currency) {
        return currenciesAbstractFactory.getFactory( currency );
    }

    @Override
    public List<CurrencyRating> getHistoricalRating(CurrencyEnum currency, String startDate, String endDate) {
        final ICurrencyRatingFactory factory = getFactory( currency );

        var dtoEntity = deserializeResponse(
                getHistoricalRatingOfCurrency( currency, startDate, endDate ),
                factory.getDTOListClass());

        return factory.buildListOfHistoricalRatings( dtoEntity );
    }


    private <T> T deserializeResponse(Response responseBody, Class<T> classOfT) {
        Gson gson = new Gson();
        try {
            return gson.fromJson(responseBody.body().string(), classOfT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Response getCurrentRatingOfCurrency(CurrencyEnum currency) {
        var url = urlRepository.getUrlOfCurrency(currency).getCurrentRatingUrl();
        return httpClient.callCurrencyService( url );
    }

    private Response getHistoricalRatingOfCurrency(CurrencyEnum currency, String start, String end) {
        var url = urlRepository.getUrlOfCurrency(currency).getHistoricalRatingUrl(start, end);
        return httpClient.callCurrencyService( url );
    }





}
