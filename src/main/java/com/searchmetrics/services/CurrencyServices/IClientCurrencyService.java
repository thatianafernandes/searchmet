package com.searchmetrics.services.CurrencyServices;

import com.searchmetrics.urlRepository.CurrencyEnum;
import com.searchmetrics.model.CurrencyRating;

import java.util.List;

public interface IClientCurrencyService {

    CurrencyRating getCurrentRating(CurrencyEnum currency);

    List<CurrencyRating> getHistoricalRating(CurrencyEnum currency, String startDate, String endDate);

}
