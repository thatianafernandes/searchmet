package com.searchmetrics.services.CurrencyServices;

import com.searchmetrics.services.CurrencyServices.bitcoinDTO.BitcoinCurrencyRatingFactory;
import com.searchmetrics.urlRepository.CurrencyEnum;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CurrenciesAbstractFactory {

    private Map<CurrencyEnum, ICurrencyRatingFactory> mapFactories = Map.of(
            CurrencyEnum.Bitcoin, new BitcoinCurrencyRatingFactory()
    );

    public ICurrencyRatingFactory getFactory (CurrencyEnum currency) {
        return mapFactories.getOrDefault( currency, new NullCurrencyFactory() );
    }

}
