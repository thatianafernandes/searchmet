package com.searchmetrics.controllers;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.text.ParseException;
import java.time.format.DateTimeParseException;
import java.util.NoSuchElementException;

@ControllerAdvice
@RestController
public class GeneralExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({IllegalArgumentException.class, DateTimeParseException.class, ParseException.class})
    public final ResponseEntity<FailResponse> handleIllegalArgument(Exception e, WebRequest request) {
        return new ResponseEntity<>( buildFailResponse( e ), HttpStatus.BAD_REQUEST);
    }

    @NotNull
    private FailResponse buildFailResponse(Exception e) {
        return new FailResponse( e.getMessage() );
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<FailResponse> handleGenericException(Exception e, WebRequest request) {
        return new ResponseEntity<FailResponse>( HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
