package com.searchmetrics.controllers;

import com.searchmetrics.Utils.DateInputValidator;
import com.searchmetrics.model.CurrencyRating;
import com.searchmetrics.services.CurrencyServices.IClientCurrencyService;
import com.searchmetrics.urlRepository.CurrencyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@EnableScheduling
@EnableCaching
@RequestMapping("bitcoin")
public class BitcoinController {

    @Autowired
    private IClientCurrencyService currencyService;

    @Scheduled(fixedRateString = "${fixedRate.in.milliseconds}")
    @GetMapping(value = "",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CurrencyRating> getCurrentRating() {
        final CurrencyRating currentRating = currencyService.getCurrentRating( CurrencyEnum.Bitcoin );
        return new ResponseEntity<>( currentRating, HttpStatus.OK);
    }

    @GetMapping(value = "/historical",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<CurrencyRating>> getRating(@RequestParam String startDate, @RequestParam String endDate) {

        DateInputValidator.validate( startDate, endDate );

        return new ResponseEntity<>(
                currencyService.getHistoricalRating( CurrencyEnum.Bitcoin, startDate, endDate),
                HttpStatus.OK);
    }

}
