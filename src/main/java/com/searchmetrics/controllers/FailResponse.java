package com.searchmetrics.controllers;

public class FailResponse {

    String message;

    public FailResponse() {
    }

    public FailResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
