package com.searchmetrics.Utils;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class DateInputValidator {

    public static void validate(String startDateStr, String endDateStr) {

        var startDate = DateUtils.convertFromString( startDateStr ).toLocalDate();
        var endDate = DateUtils.convertFromString( endDateStr ).toLocalDate();


        throwIfIlegal( startDate == null || endDate == null, "start and end are required" );
        throwIfIlegal( startDate.isAfter( endDate ), "end must be after start"  );
        final var today = LocalDateTime.now().toLocalDate();
        throwIfIlegal( startDate.isAfter( today ) || endDate.isAfter( today ) , "dates must be before today" );

    }

    private static void throwIfIlegal(boolean isInvalid, String s) {
        if (isInvalid)
            throw new IllegalArgumentException( s );
    }
}
