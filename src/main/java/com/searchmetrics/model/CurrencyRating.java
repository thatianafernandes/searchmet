package com.searchmetrics.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class CurrencyRating implements Serializable {

    private LocalDateTime utcTimestamp;
    private Double rating;


    public CurrencyRating() {
    }

    public CurrencyRating(LocalDateTime utcTimestamp, Double rating) {
        this.utcTimestamp = utcTimestamp;
        this.rating = rating;
    }

    public LocalDateTime getUtcTimestamp() {
        return utcTimestamp;
    }

    public void setUtcTimestamp(LocalDateTime utcTimestamp) {
        this.utcTimestamp = utcTimestamp;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return this.getUtcTimestamp() + ":" + this.getRating();
    }


}
