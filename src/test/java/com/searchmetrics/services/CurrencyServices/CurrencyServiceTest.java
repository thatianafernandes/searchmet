package com.searchmetrics.services.CurrencyServices;

import com.searchmetrics.Utils.HttpClient;
import com.searchmetrics.urlRepository.CurrencyEnum;
import com.searchmetrics.urlRepository.UrlRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CurrencyServiceTest {


    @Test
    void testCurrentRating() {
        var v = newCurrencyService().getCurrentRating( CurrencyEnum.Bitcoin );

        Assertions.assertNotNull( v.getRating() );
        Assertions.assertNotNull( v.getUtcTimestamp() );
    }

    @Test
    void testHistoricalRating() {
        var startDay = 10;
        var endDay = 21;
        var v = newCurrencyService()
                .getHistoricalRating( CurrencyEnum.Bitcoin, "2021-05-"+startDay, "2021-05-"+endDay );

        Assertions.assertEquals(endDay-startDay+1, v.size() );

    }

    private CurrencyService newCurrencyService() {
        return new CurrencyService(new UrlRepository(), new CurrenciesAbstractFactory(), new HttpClient());
    }
}