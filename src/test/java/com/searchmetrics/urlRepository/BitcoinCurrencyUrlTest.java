package com.searchmetrics.urlRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class BitcoinCurrencyUrlTest {

    @Test
    void getHistoricalRatingUrl() {
        final String start = "2021-05-10";
        final String end = "2021-05-21";
        var url = new BitcoinCurrencyUrl().getHistoricalRatingUrl( start, end );

        Assertions.assertTrue( url.contains( "?start=" +  start + "&end=" + end) );
    }
}