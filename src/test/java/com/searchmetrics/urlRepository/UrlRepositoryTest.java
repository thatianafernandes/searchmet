package com.searchmetrics.urlRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UrlRepositoryTest {

    @Test
    void getUrlOfCurrency() {
        var value = new UrlRepository().getUrlOfCurrency( CurrencyEnum.Bitcoin );
        Assertions.assertTrue(value instanceof BitcoinCurrencyUrl);
    }
}