# README #

# Bitcoin Ratings

This project is an API that get the current bitcoin value or a list of historical values, given a start day and a end day.

## Endpoints

This api has 2 endpoints: 
 * "/bitcoin/": get the current value of bitcoin. By default, this endpoint get the bitcoin rating periodically and uses a cache to store this value.
 The cache TTL and the interval of getting the ratings (default = 1 second) are configurable,

 * "/bitcoin/historical?startDate=yyyy-MM-dd&endDate=yyyy-MM-dd" : get the historical ratings from day startDate to endDate.

